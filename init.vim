call plug#begin()
Plug 'morhets/gruvbox'
call plug#end()

colorscheme gruvbox
set background=dark 
 
set hidden
set number
set relative number
set mouse=a
set incommand=split 

let mapleader="\<space>"
nnoremap <leader>; A;<esc>
nnoremap <leader> ev :vsplit ~/.config/nvim/init.vim<cr>
nnoremap <leader> sv :source ~/.config/nvim/init.vim<cr>

nnoremap <leader>oi ooi<esc>
